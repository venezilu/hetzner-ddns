FROM alpine:latest

#Configure
RUN apk add --no-cache curl
RUN apk add --no-cache bind-tools
RUN apk add --no-cache jq
RUN apk add --no-cache bash

RUN curl -o /srv/hetzner-dyndns_alpine.sh https://raw.githubusercontent.com/FarrowStrange/hetzner-api-dyndns/966115322b40df8fc642eff9b8f53fa6b2b07304/dyndns.sh \
    && chmod +x /srv/hetzner-dyndns_alpine.sh

#Cronjobs
RUN echo '*/5     *       *       *       *       /bin/bash -c "HETZNER_RECORD_TYPE=AAAA /srv/hetzner-dyndns_alpine.sh" > /dev/fd/1' >> /root/crontab.conf
RUN echo '*/5     *       *       *       *       /bin/bash -c "HETZNER_RECORD_TYPE=A /srv/hetzner-dyndns_alpine.sh" > /dev/fd/1' >> /root/crontab.conf

# Add the cron job
RUN crontab /root/crontab.conf

# Run the command on container startup
CMD [ "/usr/sbin/crond", "-f", "-d8" ]
